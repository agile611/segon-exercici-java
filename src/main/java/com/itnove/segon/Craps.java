package com.itnove.segon;

/**
 * Modela el joc Craps.
 *
 * @author Guillem Hernández Sola
 * @version 27/10/2017
 */
public class Craps {

    /**
     * Menú principal.
     *
     * @param args No emprats
     */
    public static void main(String args[]) {
    }

    /**
     * Executa 1000 partides i mostra la probabilitat de guanyar en tant per cent.
     *
     * @param np El nombre de partides a jugar
     */
    private static void jugarPartides(int np) {

    }

    /**
     * Mostra l'ajuda en línia.
     */
    private static void mostrarAjuda() {
    }

    /**
     * Executa i mostra el desenvolupament d'una partida.
     *
     * @return true si s'ha guanyat la partida i false en cas contrari
     */
    private static boolean jugarPartida() {
        return true;
    }

    /**
     * Executa un llançament de dos daus.
     *
     * @return La suma dels punts de les dues cares dels daus llançats
     */
    private static int llançarDaus() {
        return 0;
    }
}

