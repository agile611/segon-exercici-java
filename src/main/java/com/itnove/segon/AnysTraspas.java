package com.itnove.segon;

/**
 * Anys de traspàs.
 *
 * @author Guillem Hernández Sola
 * @version 27/10/2017
 */
public class AnysTraspas {

    /**
     * Menú principal.
     *
     * @param args No emprats
     */
    public static void main(String args[]) {

    }

    /**
     * Mostra l'ajuda en línia.
     */
    public static void mostrarAjuda() {

    }

    /**
     * Llegeix els anys inicial i final i escriu els anys que són de trapàs que hi ha entre els dons anys.
     */
    public static void anysTraspas() {

    }

    /**
     * Valida si un any és de traspàs.
     *
     * @param any Un any en l'interval [1585,3000]
     * @return true si l'any és de trapàs i false en cas contrari
     */
    public static boolean esAnyTraspas(int any) {
        return true;
    }
}
